import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'json_fut_cal.dart'; // JsonFutCal class for returnet api struct

void main() {
  runApp(MyApp());
}

// Método para obter a informação da api e converter para a estrutura de classes
Future<JsonFutCal> fetchFutCal(String url) async {
  url = Uri.encodeFull(url);
  final response = await http.get(url);
  //print(jsonDecode(response.body));
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    //return JsonFutCal.fromJson(jsonDecode(response.body));
    //print(response.body);
    return JsonFutCal.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Futebol na Televisão',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Futebol na Televisão'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Future<JsonFutCal> futureJsonFutcal;
  String url = "http://testes.optigest.net/phpspeed/arena/guia_api.php?";
  String diaMesFilter = "";

  void _toggleTab() {
    setState(() {
      String currentUrl = "";
      if (diaMesFilter != "") {
        currentUrl = url + "filters_dia_mes=" + diaMesFilter;
      } else {
        currentUrl = url;
      }
      //print(currentUrl);
      futureJsonFutcal = fetchFutCal(currentUrl);
    });
  }

  @override
  void initState() {
    super.initState();
    String currentUrl = "";
    if (diaMesFilter != "") {
      currentUrl = url + "filters[dia_mes]=" + diaMesFilter;
    } else {
      currentUrl = url;
    }
    //print(currentUrl);
    futureJsonFutcal = fetchFutCal(currentUrl);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            FutureBuilder<JsonFutCal>(
              future: futureJsonFutcal,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    children: [
                      Container(
                        decoration: new BoxDecoration(color: Colors.blue[300]),
                        child: DefaultTabController(
                          length: snapshot.data.filters.diaMes.length,
                          child: TabBar(
                            onTap: (index) {
                              diaMesFilter =
                                  snapshot.data.filters.diaMes[index];
                              _toggleTab();
                            },
                            tabs: <Widget>[
                              for (var item in snapshot.data.filters.diaMes)
                                Tab(
                                    icon: Icon(Icons.calendar_today),
                                    text: item)
                            ],
                            isScrollable: true,
                            //unselectedLabelColor: Colors.blue,
                          ),
                        ),
                      ),
                      ConstrainedBox(
                        constraints: BoxConstraints(
                            minWidth: MediaQuery.of(context).size.width),
                        child: DataTable(
                          columns: [
                            DataColumn(label: Text('Hora')),
                            DataColumn(label: Text('Visitado')),
                            DataColumn(label: Text('Visitante')),
                            DataColumn(label: Text('Canal')),
                          ],
                          rows: [
                            for (var item in snapshot.data.content)
                              DataRow(
                                cells: [
                                  DataCell(Text(item.hora)),
                                  DataCell(Text(item.visitado)),
                                  DataCell(Text(item.visitante)),
                                  DataCell(Text(item.canal)),
                                ],
                              ),
                          ],
                        ),
                      ),
                    ],
                  );
                } else if (snapshot.hasError) {
                  return Text("${snapshot.error}");
                }
                // By default, show a loading spinner.
                return CircularProgressIndicator();
              },
            )
          ],
        ),
      ),
    );
  }
}
