class JsonFutCal {
  Filters filters;
  List<Content> content;

  JsonFutCal({this.filters, this.content});

  JsonFutCal.fromJson(Map<String, dynamic> json) {
    filters =
        json['filters'] != null ? new Filters.fromJson(json['filters']) : null;
    if (json['content'] != null) {
      content = new List<Content>();
      json['content'].forEach((v) {
        content.add(new Content.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.filters != null) {
      data['filters'] = this.filters.toJson();
    }
    if (this.content != null) {
      data['content'] = this.content.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Filters {
  List<String> diaMes;

  Filters({this.diaMes});

  Filters.fromJson(Map<String, dynamic> json) {
    diaMes = json['dia_mes'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dia_mes'] = this.diaMes;
    return data;
  }
}

class Content {
  String diaSemana;
  String diaMS;
  String hora;
  String visitado;
  String visitante;
  String canal;

  Content(
      {this.diaSemana,
      this.diaMS,
      this.hora,
      this.visitado,
      this.visitante,
      this.canal});

  Content.fromJson(Map<String, dynamic> json) {
    diaSemana = json['Dia Semana'];
    diaMS = json['Dia Mês'];
    hora = json['Hora'];
    visitado = json['Visitado'];
    visitante = json['Visitante'];
    canal = json['Canal'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Dia Semana'] = this.diaSemana;
    data['Dia Mês'] = this.diaMS;
    data['Hora'] = this.hora;
    data['Visitado'] = this.visitado;
    data['Visitante'] = this.visitante;
    data['Canal'] = this.canal;
    return data;
  }
}
